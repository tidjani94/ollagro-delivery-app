package net.coodiv.ollagrodelivery.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PrefManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context context;

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file title
    private static final String PREF_NAME = "ollagro_delivery";

    private static final String APP_LANG = "app_language_pref";
    private static final String DEFAULT_LANG = "fr";
    private static final String IS_LOGGED_IN = "is_logged_in";
    private static final String SESSION_TOKEN = "session_token";
    private static final String USERNAME = "username";
    private static final String NAME = "name";
    private static final String PHONE = "phone";

    public PrefManager(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public String getSelectedLocale() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String language = preferences.getString(APP_LANG, DEFAULT_LANG);

        return language;
    }

    public void setLocale(String lang) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor;
        editor = preferences.edit();
        editor.putString(APP_LANG, lang);
        editor.commit();
    }

    public void setLoggedIn(boolean isFirstTime) {
        editor.putBoolean(IS_LOGGED_IN, isFirstTime);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGGED_IN, false);
    }

    public void setSessionToken(String sessionToken) {
        editor.putString(SESSION_TOKEN, sessionToken);
        editor.commit();
    }

    public String getSessionToken() {
        return pref.getString(SESSION_TOKEN, null);
    }

    public void setUsername(String username) {
        editor.putString(USERNAME, username);
        editor.commit();
    }

    public String getUsername() {
        return pref.getString(USERNAME, null);
    }

    public void setName(String name) {
        editor.putString(NAME, name);
        editor.commit();
    }

    public String getName() {
        return pref.getString(NAME, null);
    }

    public void setPhone(String phone) {
        editor.putString(PHONE, phone);
        editor.commit();
    }

    public String getPhone() {
        return pref.getString(PHONE, null);
    }
}