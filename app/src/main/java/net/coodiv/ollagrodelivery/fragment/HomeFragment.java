package net.coodiv.ollagrodelivery.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.activity.LoginActivity;
import net.coodiv.ollagrodelivery.activity.OrderDetailsActivity;
import net.coodiv.ollagrodelivery.adapter.OrderAdapter;
import net.coodiv.ollagrodelivery.helper.AuthManager;
import net.coodiv.ollagrodelivery.helper.RecyclerItemClickListener;
import net.coodiv.ollagrodelivery.model.Order;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment {

    private static final int ORDER_DETAILS_REQUEST_CODE = 1;
    private RecyclerView recyclerView;
    private ProgressBar progress;

    private OrderAdapter mAdapter;
    private PrefManager prefManager;
    private List<Order> myOrders, loadedOrders;
    private RequestQueue queue;
    private Gson gson;
    private Context mContext;
    private Boolean isScrolling = false;
    private Boolean ended = false;
    private int currentItems, totalItems, scrollOutItems, currentPage;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_orders_list, container, false);

        mContext = getActivity();
        prefManager = new PrefManager(mContext);
        AuthManager authManager = new AuthManager(mContext, getActivity());
        queue = Volley.newRequestQueue(mContext);
        gson = new Gson();

        String languageToLoad = prefManager.getSelectedLocale(); // your language
        Locale locale;
        if (languageToLoad.equals("ar")) {
            locale = new Locale("ar", "DZ");
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Tajawal.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        } else {
            locale = new Locale(languageToLoad);
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Montserrat.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
        } else {
            startActivity(new Intent(mContext, LoginActivity.class));
            getActivity().finish();
        }

        recyclerView = mView.findViewById(R.id.recycler_view);
        progress = mView.findViewById(R.id.progress);

        currentPage = 1;
        myOrders = new ArrayList<>();
        mAdapter = new OrderAdapter(myOrders, mContext);
        recyclerView.setAdapter(mAdapter);

        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, MotionEvent e) {
                startActivityForResult(new Intent(getActivity(), OrderDetailsActivity.class)
                        .putExtra("order_id", myOrders.get(position).getId()), ORDER_DETAILS_REQUEST_CODE);
            }
        }));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = mLayoutManager.getChildCount();
                totalItems = myOrders.size();
                scrollOutItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    currentPage++;
                    if (!ended) {
                        getOrdersList();
                    }
                }
            }
        });

        getOrdersList();

        return mView;
    }

    private void getOrdersList() {
        progress.setVisibility(View.VISIBLE);

        String url = getString(R.string.server_host_api) + "orders?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken()
                + "&lang=" + prefManager.getSelectedLocale()
                + "&page=" + currentPage;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        try {
                            loadedOrders = gson.fromJson(response, new TypeToken<List<Order>>() {
                            }.getType());
                        } catch (Exception exception) {
                            loadedOrders = new ArrayList<>();
                        }

                        if (loadedOrders.size() == 0) {
                            ended = true;
                        }

                        myOrders.addAll(loadedOrders);

                        if (myOrders.size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                        }

                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
            }
        });

        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ORDER_DETAILS_REQUEST_CODE && resultCode == RESULT_OK) {
            currentPage = 1;
            myOrders.removeAll(myOrders);
            ended = false;
            getOrdersList();
        }
    }
}
