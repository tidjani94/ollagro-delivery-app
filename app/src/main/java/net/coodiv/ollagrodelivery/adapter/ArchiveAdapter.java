package net.coodiv.ollagrodelivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.model.Archive;

import java.util.List;

public class ArchiveAdapter extends RecyclerView.Adapter<ArchiveAdapter.MyViewHolder> {

    private List<Archive> productList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, price, count, amount;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.archive_row_name);
            price = view.findViewById(R.id.archive_row_price);
            count = view.findViewById(R.id.archive_row_count);
            amount = view.findViewById(R.id.archive_row_amount);
        }
    }


    public ArchiveAdapter(List<Archive> productList, Context mContext) {
        this.productList = productList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_archive, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Archive archive = productList.get(position);
            holder.name.setText(archive.getProductUnitTaste().getProductUnit().getProduct().getName() + " - " + archive.getProductUnitTaste().getTaste().getName());
        holder.price.setText(String.valueOf(archive.getUnitPrice()));
        holder.amount.setText(String.valueOf(archive.getAmount()));
        holder.count.setText(archive.getAmount() * archive.getUnitPrice() + " " + mContext.getString(R.string.currency));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
