package net.coodiv.ollagrodelivery.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragmentList;
    private FragmentManager fragmentManager;

    public ScreenSlidePagerAdapter(FragmentManager fragmentManager, List<Fragment> fragmentList) {
        super(fragmentManager);
        this.fragmentList = fragmentList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public Fragment getItem(int i) {
        if (i >= 0 && i < fragmentList.size()){
            return fragmentList.get(i);
        }
        return null;
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
