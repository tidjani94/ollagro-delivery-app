package net.coodiv.ollagrodelivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.model.Order;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    private List<Order> orderList;
    private Context mContext;
    private String url;
    private PrefManager prefManager;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView orderId, total, date, state, town;
        private View pendding, confirmed, readyToDeliver, canceled, delivered;

        public MyViewHolder(View view) {
            super(view);
            orderId = view.findViewById(R.id.order_address);
            total = view.findViewById(R.id.order_total);
            date = view.findViewById(R.id.date_picker);
            state = view.findViewById(R.id.order_state);
            pendding = view.findViewById(R.id.pendding);
            confirmed = view.findViewById(R.id.confirmed);
            readyToDeliver = view.findViewById(R.id.ready_to_deliver);
            canceled = view.findViewById(R.id.canceled);
            delivered = view.findViewById(R.id.delivered);
            town = view.findViewById(R.id.town);
        }
    }


    public OrderAdapter(List<Order> orderList, Context mContext) {
        this.orderList = orderList;
        this.mContext = mContext;
        this.prefManager = new PrefManager(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_order, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Order order = orderList.get(position);
        holder.orderId.setText(String.valueOf("#" + order.getId()));
        holder.total.setText(String.valueOf(order.getTotal()));
        holder.date.setText(order.getDeliveryDate());
        holder.town.setText(order.getClient().getTown().getName());
        holder.state.setText(mContext.getResources().getStringArray(R.array.order_state)[order.getState()]);
        switch (order.getState()) {
            case 0:
                holder.pendding.setVisibility(View.VISIBLE);
                holder.confirmed.setVisibility(View.GONE);
                holder.readyToDeliver.setVisibility(View.GONE);
                holder.canceled.setVisibility(View.GONE);
                holder.delivered.setVisibility(View.GONE);
                break;
            case 1:
                holder.pendding.setVisibility(View.GONE);
                holder.confirmed.setVisibility(View.VISIBLE);
                holder.readyToDeliver.setVisibility(View.GONE);
                holder.canceled.setVisibility(View.GONE);
                holder.delivered.setVisibility(View.GONE);
                break;
            case 2:
                holder.pendding.setVisibility(View.GONE);
                holder.confirmed.setVisibility(View.GONE);
                holder.readyToDeliver.setVisibility(View.VISIBLE);
                holder.canceled.setVisibility(View.GONE);
                holder.delivered.setVisibility(View.GONE);
                break;
            case 3:
                holder.pendding.setVisibility(View.GONE);
                holder.confirmed.setVisibility(View.GONE);
                holder.readyToDeliver.setVisibility(View.GONE);
                holder.canceled.setVisibility(View.VISIBLE);
                holder.delivered.setVisibility(View.GONE);
                break;
            case 4:
                holder.pendding.setVisibility(View.GONE);
                holder.confirmed.setVisibility(View.GONE);
                holder.readyToDeliver.setVisibility(View.GONE);
                holder.canceled.setVisibility(View.GONE);
                holder.delivered.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
}
