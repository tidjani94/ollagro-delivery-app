package net.coodiv.ollagrodelivery.model;

import com.google.gson.annotations.SerializedName;

public class Archive {
    private int orders;
    @SerializedName("product_unit_taste")
    private ProductUnitTaste productUnitTaste;
    private int amount;
    @SerializedName("unit_price")
    private int unitPrice;

    public Archive() {
    }

    public Archive(int orders, ProductUnitTaste productUnitTaste, int amount, int unitPrice) {
        this.orders = orders;
        this.productUnitTaste = productUnitTaste;
        this.amount = amount;
        this.unitPrice = unitPrice;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public ProductUnitTaste getProductUnitTaste() {
        return productUnitTaste;
    }

    public void setProductUnitTaste(ProductUnitTaste productUnitTaste) {
        this.productUnitTaste = productUnitTaste;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }
}
