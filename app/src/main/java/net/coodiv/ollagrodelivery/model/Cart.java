package net.coodiv.ollagrodelivery.model;

public class Cart {
    private ProductUnitTaste productUnitTaste;
    private int amount;

    public Cart() {
    }

    public Cart(ProductUnitTaste productUnitTaste, int amount) {
        this.productUnitTaste = productUnitTaste;
        this.amount = amount;
    }

    public ProductUnitTaste getProductUnitTaste() {
        return productUnitTaste;
    }

    public void setProductUnitTaste(ProductUnitTaste productUnitTaste) {
        this.productUnitTaste = productUnitTaste;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
