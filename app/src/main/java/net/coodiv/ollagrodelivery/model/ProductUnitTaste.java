package net.coodiv.ollagrodelivery.model;

import com.google.gson.annotations.SerializedName;

public class ProductUnitTaste {
    private int id;
    @SerializedName("product_unit")
    private ProductUnit productUnit;
    private Taste taste;
    private double price;
    private double amount;

    public ProductUnitTaste() {
    }

    public ProductUnitTaste(int id, ProductUnit productUnit, Taste taste, double price, double amount) {
        this.id = id;
        this.productUnit = productUnit;
        this.taste = taste;
        this.price = price;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductUnit getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(ProductUnit productUnit) {
        this.productUnit = productUnit;
    }

    public Taste getTaste() {
        return taste;
    }

    public void setTaste(Taste taste) {
        this.taste = taste;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
