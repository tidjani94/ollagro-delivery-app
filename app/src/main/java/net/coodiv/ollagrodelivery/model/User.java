package net.coodiv.ollagrodelivery.model;

public class User {
    private int id;
    private String name;
    private String username;
    private String phone;
    private String token;
    private Region region;

    public User() {
    }

    public User(int id, String name, String username, String phone, String token, Region region) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.phone = phone;
        this.token = token;
        this.region = region;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
}
