package net.coodiv.ollagrodelivery.model;

public class ProductUnit {
    private int id;
    private Product product;
    private Unit unit;

    public ProductUnit() {
    }

    public ProductUnit(int id, Product product, Unit unit) {
        this.id = id;
        this.product = product;
        this.unit = unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
