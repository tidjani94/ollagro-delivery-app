package net.coodiv.ollagrodelivery.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Order {
    private int id;
    private int state;
    @SerializedName("delivery_date")
    private String deliveryDate;
    @SerializedName("order_date")
    private String orderDate;
    private List<Archive> archives;
    private double total;
    private Client client;
    private int employee;

    public Order() {
    }

    public Order(int id, int state, String deliveryDate, String orderDate, List<Archive> archives, double total, Client client, int employee) {
        this.id = id;
        this.state = state;
        this.deliveryDate = deliveryDate;
        this.orderDate = orderDate;
        this.archives = archives;
        this.total = total;
        this.client = client;
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public List<Archive> getArchives() {
        return archives;
    }

    public void setArchives(List<Archive> archives) {
        this.archives = archives;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public int getEmployee() {
        return employee;
    }

    public void setEmployee(int employee) {
        this.employee = employee;
    }
}
