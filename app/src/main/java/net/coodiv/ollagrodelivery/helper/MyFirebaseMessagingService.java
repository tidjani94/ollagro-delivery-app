package net.coodiv.ollagrodelivery.helper;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ahmed Tidjani on 02-Oct-17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private PrefManager prefManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String click_action = remoteMessage.getNotification().getClickAction();
        Intent intent = new Intent(click_action);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle(remoteMessage.getNotification().getTitle());
        notificationBuilder.setContentText(remoteMessage.getNotification().getBody());
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSmallIcon(R.drawable.ic_logo_ollagro);
        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher));
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onNewToken(final String appToken) {
        super.onNewToken(appToken);

        prefManager = new PrefManager(this);

        RequestQueue queue = Volley.newRequestQueue(this);

        String url = getString(R.string.server_host_api) + "update_app_token";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("fcm_token", appToken);

                return params;
            }
        };
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }
}
