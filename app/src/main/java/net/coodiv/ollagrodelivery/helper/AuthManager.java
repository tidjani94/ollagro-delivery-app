package net.coodiv.ollagrodelivery.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;


import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.activity.LoginActivity;
import net.coodiv.ollagrodelivery.model.User;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ahmed Tidjani on 06-Aug-17.
 */

public class AuthManager {

    private Context mContext;
    private Activity activity;
    private String url;
    private PrefManager prefManager;
    private User mUser;
    private Gson gson;
    private RequestQueue queue;
    private String appToken;

    public AuthManager(Context mContext, Activity activity) {
        this.mContext = mContext;
        this.prefManager = new PrefManager(mContext);
        this.activity = activity;
        this.gson = new Gson();
        this.queue = Volley.newRequestQueue(mContext);
    }

    public void isLoggedIn() {

        this.url = mContext.getString(R.string.server_host_api)
                + "auth?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mUser = gson.fromJson(response.trim(), User.class);
                        prefManager.setSessionToken(mUser.getToken().trim());
                        prefManager.setLoggedIn(true);
                        prefManager.setName(mUser.getName());
                        prefManager.setUsername(mUser.getUsername());
                        prefManager.setPhone(mUser.getPhone());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    switch (networkResponse.statusCode) {
                        case HttpURLConnection.HTTP_UNAUTHORIZED:
                            logout();
                            break;
                        case HttpURLConnection.HTTP_FORBIDDEN:
                            logout();
                            break;
                        default:
                            break;
                    }
                }
            }
        });

        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public void logout() {
        prefManager.setSessionToken(null);
        prefManager.setLoggedIn(false);
        prefManager.setUsername(null);
        prefManager.setName(null);
        prefManager.setPhone(null);

        mContext.startActivity(new Intent(mContext, LoginActivity.class));
        activity.finish();
    }

    public void refreshAppToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        appToken = task.getResult().getToken();

                        RequestQueue queue = Volley.newRequestQueue(mContext);

                        String url = mContext.getString(R.string.server_host_api) + "update_app_token";
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put("username", prefManager.getUsername());
                                params.put("token", prefManager.getSessionToken());
                                params.put("fcm_token", appToken);

                                return params;
                            }
                        };
                        stringRequest.setShouldCache(false);
                        queue.add(stringRequest);
                    }
                });
    }
}
