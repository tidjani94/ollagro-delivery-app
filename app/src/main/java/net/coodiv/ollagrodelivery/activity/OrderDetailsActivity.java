package net.coodiv.ollagrodelivery.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.adapter.ArchiveAdapter;
import net.coodiv.ollagrodelivery.helper.AuthManager;
import net.coodiv.ollagrodelivery.model.Order;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderDetailsActivity extends AppCompatActivity {

    private TextView orderState, orderNumber, total, deliverDate, orderTown;
    private RecyclerView recyclerView;
    private ProgressBar progress;
    private TextView title;
    private Toolbar toolbar;
    private ImageButton location;

    private PrefManager prefManager;
    private RequestQueue queue;
    private Gson gson;
    private ArchiveAdapter mAdapter;
    private ProgressDialog progressDialog;
    private Order order;
    private MenuItem acceptOrderItem, refuseOrderItem, readyToDeliverItem, deliveredItem;
    private AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        AuthManager authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        String languageToLoad = prefManager.getSelectedLocale();
        Locale locale;
        if (languageToLoad.equals("ar")) {
            locale = new Locale("ar", "DZ");
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Tajawal.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        } else {
            locale = new Locale(languageToLoad);
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Montserrat.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_order_details);

        if (prefManager.isLoggedIn()) {
            authManager.isLoggedIn();
        } else {
            finish();
        }

        toolbar = findViewById(R.id.toolbar);
        title = findViewById(R.id.title);
        orderNumber = findViewById(R.id.order_number);
        total = findViewById(R.id.order_total);
        deliverDate = findViewById(R.id.order_delivery_date);
        orderState = findViewById(R.id.order_state);
        orderTown = findViewById(R.id.order_town);
        recyclerView = findViewById(R.id.archive_recycler_view);
        progress = findViewById(R.id.progress);
        location = findViewById(R.id.location_ib);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        title.setText(getString(R.string.order_details));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (getIntent().getExtras() != null) {
            int orderId = getIntent().getExtras().getInt("order_id");
            getOrderDetails(orderId);
        }

        location.setVisibility(View.INVISIBLE);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q="
                        + order.getClient().getLatitude()
                        + ","
                        + order.getClient().getLongitude());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });
    }

    private void getOrderDetails(final int orderId) {
        String url = getString(R.string.server_host_api) + "orders/" + orderId
                + "?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken()
                + "&lang=" + prefManager.getSelectedLocale();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            order = gson.fromJson(response, Order.class);
                            setupMenu();
                        } catch (Exception exception) {
                            Toast.makeText(OrderDetailsActivity.this, getString(R.string.somthing_wrong), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        setupOrderInfos();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrderDetailsActivity.this, getString(R.string.somthing_wrong), Toast.LENGTH_SHORT).show();
            }
        });

        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    private void setupOrderInfos() {
        orderNumber.setText("#" + order.getId());
        total.setText(String.valueOf(order.getTotal()));
        deliverDate.setText(order.getDeliveryDate());
        orderState.setText(getResources().getStringArray(R.array.order_state)[order.getState()]);
        orderTown.setText(order.getClient().getTown().getName());
        location.setVisibility(View.VISIBLE);

        getArchiveProducts();
    }

    public void getArchiveProducts() {

        mAdapter = new ArchiveAdapter(order.getArchives(), OrderDetailsActivity.this);
        recyclerView.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();
        progress.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.order_details_menu, menu);

        acceptOrderItem = menu.findItem(R.id.action_accept_order);
        refuseOrderItem = menu.findItem(R.id.action_refuse_order);
        readyToDeliverItem = menu.findItem(R.id.action_ready_to_deliver);
        deliveredItem = menu.findItem(R.id.action_delivered);

        acceptOrderItem.setVisible(false);
        refuseOrderItem.setVisible(false);
        readyToDeliverItem.setVisible(false);
        deliveredItem.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this);
        switch (item.getItemId()) {
            case R.id.action_accept_order:
                builder.setTitle(getString(R.string.accept_order)).setMessage(getString(R.string.accept_order_q)).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        acceptOrder();
                        dialog.dismiss();
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert = builder.create();
                alert.show();
                break;
            case R.id.action_refuse_order:
                builder.setTitle(getString(R.string.refuse_order)).setMessage(getString(R.string.accept_order_q)).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        refuseOrder();
                        dialog.dismiss();
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert = builder.create();
                alert.show();
                break;
            case R.id.action_ready_to_deliver:
                builder.setTitle(getString(R.string.ready_to_deliver)).setMessage(getString(R.string.ready_to_deliver_order_q)).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        setOrderReadyToDeliver();
                        dialog.dismiss();
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert = builder.create();
                alert.show();
                break;
            case R.id.action_delivered:
                builder.setTitle(getString(R.string.delivered)).setMessage(getString(R.string.delivered_order_q)).setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        setOrderDelivered();
                        dialog.dismiss();
                    }
                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert = builder.create();
                alert.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setOrderReadyToDeliver() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "orders/confirmed/" + order.getId();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Toast.makeText(OrderDetailsActivity.this, getString(R.string.ready_to_deliver), Toast.LENGTH_SHORT).show();
                        order = gson.fromJson(response, Order.class);
                        setupMenu();
                        setupOrderInfos();
                        setResult(RESULT_OK);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(OrderDetailsActivity.this, getString(R.string.somthing_wrong), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("lang", prefManager.getSelectedLocale());

                return params;
            }
        };
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    private void setOrderDelivered() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "orders/ready/" + order.getId();

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Toast.makeText(OrderDetailsActivity.this, getString(R.string.delivered), Toast.LENGTH_SHORT).show();
                        order = gson.fromJson(response, Order.class);
                        setupMenu();
                        setupOrderInfos();
                        setResult(RESULT_OK);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(OrderDetailsActivity.this, getString(R.string.somthing_wrong), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("lang", prefManager.getSelectedLocale());

                return params;
            }
        };
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    private void acceptOrder() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = "";
        if (order.getEmployee() == 0) {
            url = getString(R.string.server_host_api) + "orders/" + order.getId();
        } else {
            url = getString(R.string.server_host_api) + "orders/assigned/" + order.getId();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Toast.makeText(OrderDetailsActivity.this, getString(R.string.order_accepted), Toast.LENGTH_SHORT).show();
                        order = gson.fromJson(response, Order.class);
                        setupMenu();
                        setupOrderInfos();
                        setResult(RESULT_OK);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(OrderDetailsActivity.this, getString(R.string.somthing_wrong), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("lang", prefManager.getSelectedLocale());

                return params;
            }
        };
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    private void refuseOrder() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "orders/" + order.getId()
                + "?username=" + prefManager.getUsername()
                + "&token=" + prefManager.getSessionToken()
                + "&lang=" + prefManager.getSelectedLocale();

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Toast.makeText(OrderDetailsActivity.this, getString(R.string.order_refuserd), Toast.LENGTH_SHORT).show();
                        order = gson.fromJson(response, Order.class);
                        setupMenu();
                        setupOrderInfos();
                        setResult(RESULT_OK);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(OrderDetailsActivity.this, getString(R.string.somthing_wrong), Toast.LENGTH_SHORT).show();
            }
        });
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    private void setupMenu() {
        acceptOrderItem.setVisible(false);
        refuseOrderItem.setVisible(false);
        readyToDeliverItem.setVisible(false);
        deliveredItem.setVisible(false);
        switch (order.getState()) {
            case 0:
                acceptOrderItem.setVisible(true);
                if (order.getEmployee() != 0) {
                    refuseOrderItem.setVisible(true);
                }
                break;
            case 1:
                readyToDeliverItem.setVisible(true);
                break;
            case 2:
                deliveredItem.setVisible(true);
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
