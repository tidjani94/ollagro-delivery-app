package net.coodiv.ollagrodelivery.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.gauravk.bubblenavigation.BubbleNavigationConstraintView;
import com.gauravk.bubblenavigation.listener.BubbleNavigationChangeListener;
import com.google.gson.Gson;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.adapter.ScreenSlidePagerAdapter;
import net.coodiv.ollagrodelivery.fragment.AssignedOrdersListFragment;
import net.coodiv.ollagrodelivery.fragment.HomeFragment;
import net.coodiv.ollagrodelivery.fragment.OrdersListFragment;
import net.coodiv.ollagrodelivery.helper.AuthManager;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private static final int SELECT_LANG_REQUEST_CODE = 1;

    private TextView title;
    private BubbleNavigationConstraintView bubbleNavigationConstraintView;
    private ViewPager viewPager;
    private Toolbar toolbar;

    private PrefManager prefManager;
    private AuthManager authManager;
    private RequestQueue queue;
    private Gson gson;
    private ScreenSlidePagerAdapter screenSlidePagerAdapter;
    private List<Fragment> fragmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        String languageToLoad = prefManager.getSelectedLocale();
        Locale locale;
        if (languageToLoad.equals("ar")) {
            locale = new Locale("ar", "DZ");
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Tajawal.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        } else {
            locale = new Locale(languageToLoad);
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Montserrat.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_main);

        if (!prefManager.isLoggedIn()) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        } else {
            authManager.isLoggedIn();
            authManager.refreshAppToken();
        }

        toolbar = findViewById(R.id.toolbar);
        title = findViewById(R.id.title);
        bubbleNavigationConstraintView = findViewById(R.id.navigation_constraint);
        viewPager = findViewById(R.id.view_pager);

        setSupportActionBar(toolbar);
        title.setText(getString(R.string.home));

        fragmentList = new ArrayList<>();
        fragmentList.add(new AssignedOrdersListFragment());
        fragmentList.add(new HomeFragment());
        fragmentList.add(new OrdersListFragment());

        screenSlidePagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(screenSlidePagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                bubbleNavigationConstraintView.setCurrentActiveItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        bubbleNavigationConstraintView.setNavigationChangeListener(new BubbleNavigationChangeListener() {
            @Override
            public void onNavigationChanged(View view, int position) {
                viewPager.setCurrentItem(position);
                switch (position) {
                    case 0:
                        title.setText(getString(R.string.assigned_orders));
                        break;
                    case 1:
                        title.setText(getString(R.string.home));
                        break;
                    case 2:
                        title.setText(getString(R.string.my_orders));
                        break;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(getString(R.string.logout))
                        .setMessage(getString(R.string.logout_q))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                authManager.logout();
                                dialog.dismiss();
                            }
                        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                break;
            case R.id.action_change_password:
                startActivity(new Intent(MainActivity.this, ChangePasswordActivity.class));
                break;
            case R.id.action_about:
                startActivity(new Intent(MainActivity.this, AboutActivity.class));
                break;
            case R.id.action_change_lang:
                startActivityForResult(new Intent(MainActivity.this, ChooseLangActivity.class)
                        , SELECT_LANG_REQUEST_CODE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == SELECT_LANG_REQUEST_CODE) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
