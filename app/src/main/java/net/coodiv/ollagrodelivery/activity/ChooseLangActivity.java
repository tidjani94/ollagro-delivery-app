package net.coodiv.ollagrodelivery.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChooseLangActivity extends AppCompatActivity {

    private Spinner chooseLang;
    private TextView okBtn;

    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);

        String languageToLoad = prefManager.getSelectedLocale();
        Locale locale;
        if (languageToLoad.equals("ar")) {
            locale = new Locale("ar", "DZ");
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Tajawal.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        } else {
            locale = new Locale(languageToLoad);
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Montserrat.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_choose_lang);

        chooseLang = findViewById(R.id.choose_lang);
        okBtn = findViewById(R.id.ok_btn);

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getTextArray(R.array.language_list_array));
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        chooseLang.setAdapter(aa);
        for (int i = 0; i < getResources().getTextArray(R.array.language_list_array).length; i++) {

            if (getResources().getTextArray(R.array.language_list_values)[i].toString().trim()
                    .equals(prefManager.getSelectedLocale())) {
                chooseLang.setSelection(i);
            }
        }
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefManager.setLocale(getResources().getTextArray(R.array.language_list_values)[chooseLang.getSelectedItemPosition()].toString());
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
