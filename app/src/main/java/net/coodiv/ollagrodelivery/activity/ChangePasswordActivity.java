package net.coodiv.ollagrodelivery.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.helper.AuthManager;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText oldPassword, newPassword, reNewPassword;
    private Button edit;
    private Toolbar toolbar;
    private TextView title;

    private PrefManager prefManager;
    private AuthManager authManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        authManager = new AuthManager(this, this);

        String languageToLoad = prefManager.getSelectedLocale(); // your language
        Locale locale;
        if (languageToLoad.equals("ar")) {
            locale = new Locale("ar", "DZ");
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Tajawal.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        } else {
            locale = new Locale(languageToLoad);
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Montserrat.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_change_password);

        if (!prefManager.isLoggedIn()) {
            finish();
        } else {
            authManager.isLoggedIn();
        }

        toolbar = findViewById(R.id.toolbar);
        oldPassword = findViewById(R.id.old_password);
        newPassword = findViewById(R.id.new_password);
        reNewPassword = findViewById(R.id.re_new_password);
        edit = findViewById(R.id.edit_btn);
        title = findViewById(R.id.title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        title.setText(getString(R.string.change_password));

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (oldPassword.getText().toString().length() < 8) {
                    oldPassword.setError(getString(R.string.short_password));
                } else if (newPassword.getText().toString().length() < 8) {
                    newPassword.setError(getString(R.string.short_password));
                } else if (reNewPassword.getText().toString().length() < 8) {
                    reNewPassword.setError(getString(R.string.short_password));
                } else if (!reNewPassword.getText().toString().equals(newPassword.getText().toString())) {
                    reNewPassword.setError(getString(R.string.password_dont_match));
                } else {
                    edit.setEnabled(false);
                    changePassword();
                }
            }
        });
    }

    public void changePassword() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, null, getString(R.string.change_password), true);

        RequestQueue queue = Volley.newRequestQueue(this);

        String url = getString(R.string.server_host_api) + "password";
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                edit.setEnabled(true);
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    switch (networkResponse.statusCode) {
                        case HttpURLConnection.HTTP_UNAUTHORIZED:
                            oldPassword.setError(getString(R.string.wrong_password));
                            break;
                        default:
                            Toast.makeText(ChangePasswordActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", prefManager.getUsername());
                params.put("token", prefManager.getSessionToken());
                params.put("password", oldPassword.getText().toString());
                params.put("new_password", newPassword.getText().toString());

                return params;
            }
        };

        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
