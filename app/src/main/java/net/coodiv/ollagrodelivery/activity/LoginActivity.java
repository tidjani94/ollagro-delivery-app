package net.coodiv.ollagrodelivery.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.mikepenz.iconics.context.IconicsLayoutInflater2;

import net.coodiv.ollagrodelivery.R;
import net.coodiv.ollagrodelivery.model.User;
import net.coodiv.ollagrodelivery.preferences.PrefManager;

import java.net.HttpURLConnection;

public class LoginActivity extends AppCompatActivity {

    private EditText username, password;
    private Button login;

    private ProgressDialog connectDialog;
    private PrefManager prefManager;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        super.onCreate(savedInstanceState);

        prefManager = new PrefManager(this);
        queue = Volley.newRequestQueue(this);

        setContentView(R.layout.activity_login);

        if (prefManager.isLoggedIn()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        username = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);
        login = findViewById(R.id.btn_login);

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String result = username.getText().toString().replaceAll(" ", "");
                if (!username.getText().toString().equals(result)) {
                    username.setText(result);
                    username.setSelection(result.length());
                    // alert the user
                }
            }
        });

        if (getResources().getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            username.setGravity(Gravity.RIGHT);
            password.setGravity(Gravity.RIGHT);
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().trim().length() < 4) {
                    username.setError(getString(R.string.short_username));
                } else if (password.getText().toString().trim().length() < 8) {
                    password.setError(getString(R.string.short_password));
                } else {
                    logIn(username.getText().toString().trim(), password.getText().toString().trim());
                }
            }
        });
    }

    public void logIn(final String username, final String password) {
        connectDialog = ProgressDialog.show(this, null, getString(R.string.wait_connect_dialog), true);

        String url = getString(R.string.server_host_api) + "login?username=" + username
                + "&password=" + password;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        connectDialog.dismiss();

                        Gson gson = new Gson();
                        User mUser = gson.fromJson(response.trim(), User.class);
                        prefManager.setSessionToken(mUser.getToken().trim());
                        prefManager.setLoggedIn(true);
                        prefManager.setName(mUser.getName());
                        prefManager.setUsername(mUser.getUsername());
                        prefManager.setPhone(mUser.getPhone());

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                connectDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    switch (networkResponse.statusCode) {
                        case HttpURLConnection.HTTP_UNAUTHORIZED: {
                            Toast.makeText(LoginActivity.this, getString(R.string.username_or_password_error), Toast.LENGTH_LONG).show();
                            break;
                        }
                        case HttpURLConnection.HTTP_FORBIDDEN: {
                            Toast.makeText(LoginActivity.this, getString(R.string.inactive_account), Toast.LENGTH_LONG).show();
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                }
            }
        });
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }
}
